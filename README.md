# LM GIG

This repository contains the entire LM GIG app and is designed for AWS deployment.

## Getting Started

```git clone https://vardaro@bitbucket.org/vardaro/lmgig.git```

```npm install``` in the root directory

`cd` into the `app` directory and run

```npm install```

```ng build --prod```

In the root directory run
```node server```

and visit http://localhost:9000 to verify the app is working.

## Development

#### Server

For doing server side development, I'd recommend running `npm install -g nodemon`
to install `nodemon`, a module that makes it very easy for doing server side development.

To begin the server just run `node server`, if you installed `nodemon` then you can just type `nodemon`.

#### Client

If you wish to just contribute to the client app, `cd` into the `app` directory and run `ng serve --o`

## API

#### Routing
Upon deployment, the server must invoke `npm run start` which initiates the web server. All further api routes should be put in the `/api` directory to keep everything consistent.

When adding new routes, `export` a function that accepts the express instance as an arguement creating these routes and then `require` the corresponding file in `server.js`. An example of this can be found in `auth.js`

#### Sensitive Info

Sensitive info like API keys and such, by convention, should be declared as environment variables on the server. If there is no environment variables present (meaning you are in development) then sensitive info will be store in `/api/secret.js` which `exports` an object containing all sensitive info. This file is <b>NOT</b> for included in the version control because that would defeat the purpose.

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors({credentials: true, origin: true}));

// inject routes
require('./api/auth')(app);
require('./api/tasks')(app);

const PORT = process.env.PORT || 9000; // if local, default port to 3000

// Host the static angular files
app.use(express.static(__dirname + '/app/dist/blackboard-angular'));

// When people visit /
// send them index.html
app.get('/', (req, res) => {
    res.sendFile('./app/dist/blackboard-angular/index.html');
});

app.listen(PORT, () => {
    console.log(`Listening on ${PORT}`);
});

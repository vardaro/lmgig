"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Tracker = /** @class */ (function () {
    function Tracker() {
        this.numFinished = 0;
        this.numPosted = 0;
        this.numClaimed = 0;
        this.numCompleted = 0;
        this.numExtra = 0;
    }
    return Tracker;
}());
exports.Tracker = Tracker;
var Requirement = /** @class */ (function () {
    function Requirement(type, goal) {
        this.type = type;
        this.goal = goal;
        this.value = 0;
        this.complete = false;
    }
    Requirement.prototype.Value = function () {
        return this.value;
    };
    Requirement.prototype.Goal = function () {
        return this.goal;
    };
    Requirement.prototype.Complete = function () {
        return this.complete;
    };
    Requirement.prototype.update = function (data) {
        if (this.type == "posted") {
            this.value = data.numPosted;
        }
        if (this.type == "claimed") {
            this.value = data.numClaimed;
        }
        if (this.type == "completed") {
            this.value = data.numCompleted;
        }
        if (this.type == "extra") {
            this.value = data.numExtra;
        }
        if (this.value >= this.goal) {
            this.complete = true;
        }
    };
    return Requirement;
}());
exports.Requirement = Requirement;
var Achievement = /** @class */ (function () {
    function Achievement(title, description, image, REQUIREMENTS) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.REQUIREMENTS = REQUIREMENTS;
        this.percentage = 0;
        this.val = 0;
        this.done = 0;
        this.goal = 0;
        this.completed = false;
        for (var c = 0; c < this.REQUIREMENTS.length; c++) {
            var req = this.REQUIREMENTS[c];
            this.goal += req.Goal();
        }
    }
    Achievement.prototype.complete = function (data) {
        if (!this.completed && this.REQUIREMENTS.length == 0) {
            this.completed = true;
            data.numFinished++;
            return true;
        }
        return false;
    };
    Achievement.prototype.update = function (data) {
        if (!this.complete(data)) {
            this.val = this.done;
            for (var c = 0; c < this.REQUIREMENTS.length; c++) {
                var req = this.REQUIREMENTS[c];
                if (!req.Complete()) {
                    req.update(data);
                }
                if (req.Complete()) {
                    this.done += req.Value();
                    var index = this.REQUIREMENTS.indexOf(req);
                    this.REQUIREMENTS.splice(index, 1);
                    this.complete(data);
                }
                this.val += req.Value();
            }
            this.percentage = (this.val / this.goal) * 100;
        }
    };
    return Achievement;
}());
exports.Achievement = Achievement;
exports.ACHIEVEMENTS = [
    new Achievement('Post a task', 'Post a task.\nTime to get things done.', '../assets/achievement_post.png', [new Requirement('posted', 1)]),
    new Achievement('Adept Poster', 'Post a few tasks.\nAnd you get a task, and you get a task...', '../assets/achievement_post.png', [new Requirement('posted', 3)]),
    new Achievement('Posting Guru', 'Posting master.\nSpreading the love.', '../assets/achievement_post.png', [new Requirement('posted', 5)]),
    new Achievement('Claim a task', 'Claim a task.\nTime to get cracking!', '../assets/achievement_claim.png', [new Requirement('claimed', 1)]),
    new Achievement('Frequent Claimer', 'Claim a few tasks.\nSome serious initiative.', '../assets/achievement_claim.png', [new Requirement('claimed', 3)]),
    new Achievement('Claiming Enthusiast', 'Claiming master.\nLet\'s do this!', '../assets/achievement_claim.png', [new Requirement('claimed', 5)]),
    new Achievement('Complete a task', 'Complete a task.\nBoom baby!', '../assets/achievement_complete.png', [new Requirement('completed', 1)]),
    new Achievement('Consistant Completer', 'Complete a few tasks.\nOn to the next one.', '../assets/achievement_complete.png', [new Requirement('completed', 3)]),
    new Achievement('Completionist', 'Completing master.\nGetting things done.', '../assets/achievement_complete.png', [new Requirement('completed', 5)]),
    new Achievement('Full Process', 'Post a task. Claim a task. Complete a task.', '../assets/achievement_full_process.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1)]),
    new Achievement('Creative Genius', 'Changed the theme.\n Look at all those pretty colors.', '../assets/achievement_change_theme.png', [new Requirement('extra', 1)]),
    new Achievement('Looking Good', 'Changed your profile picture.\n Nice outfit!', '../assets/achievement_change_profile.png', [new Requirement('extra', 1)]),
    new Achievement('Slim Shady', 'The real Slim Shady.\nFind a secret.', '../assets/achievement_slim_shady.png', [new Requirement('extra', 1)]),
    new Achievement('Achieve Enlightenment', 'Do everything.', '../assets/achievement_enlightenment.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1)]),
    //Make symbol and change requirements for these achievements
    new Achievement('Padawan to Jedi', 'Complete a simple, medium, and hard task', '../assets/achievement_enlightenment.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1)]),
    new Achievement('Jack of all Trades', 'Complete a job from each of the categories', '../assets/achievement_enlightenment.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1)]),
    new Achievement('Feedback', 'Send us a message about what you think of the app!', '../assets/achievement_enlightenment.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1)]),
    new Achievement('Achieve Enlightenment', 'Do everything.', '../assets/achievement_enlightenment.png', [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1)])
];
//# sourceMappingURL=achievements.js.map
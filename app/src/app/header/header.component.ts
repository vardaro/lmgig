import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { Router } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';
import { ColorPickComponent } from '../color-pick/color-pick.component';
import { AchieveService } from '../services/achieve.service';
import { Notification } from '../notification';
import { NOTIFICATIONS } from '../mock-notif';
import { TASKS, Task } from '../tasks';
import { PushNotificationsService } from '../services/push.notification.service';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

import { User } from '../user';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: User;
  Tasks: Task[];
  tasks = TASKS;
  Notifications: Notification[];
  notifications = NOTIFICATIONS;


  constructor(
    public dialog: MatDialog,
    public router: Router,
    private auth: AuthService,
    private achieveService: AchieveService,
    private PushNotificationsService: PushNotificationsService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      "notifications",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/notifications.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "settings",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/settings.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "achievements",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/achievements.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "themes",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/themes.svg")
    );
  }

  achieved: number = 0;
  active: number = 0;
  notificationNumber: number = 0;
  notifCount: number = 0;
  pic: boolean;
  noPic: boolean;
  

  ngOnInit() {
    this.auth.user$.subscribe(user => this.user = user);
  }

  openDialog(): void {
    let dialog = this.dialog.open(DialogComponent);
  }

  openColor(): void {
    let color = this.dialog.open(ColorPickComponent);
  }

  update(): number {
    this.updateNotification();
    var num = this.achieveService.update();
    this.active = num - this.achieved;
    return num;
  }
  
  updateNotification(): void {
    var num = this.PushNotificationsService.numberOfNotificaiton();
    if (num > this.notifCount) {
      this.notificationNumber = num - this.notifCount;
      this.notifCount = num;
    }

    //used to make image work incase there its not connected to a server  
    try {
      this.user.DisplayName;
      this.noPic = true;
      this.pic = false;
    } catch{
      this.noPic = false;
      this.pic = true;
    }
  }

  viewAchievements(): void {
    this.achieved = this.update();
    this.active = 0;
  }

  restcount() {
    this.update();
    this.notificationNumber = 0;
  }

  clearNotification(): void {
    this.PushNotificationsService.clear();
  }
}

import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { Login } from '../login';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  login: Login = {
    username: "",
    password: ""
  };
  data: User;

  constructor(public dialog: MatDialog,
    private auth: AuthService,
    private router: Router) { }

  openDialog(): void {
    let dialogRef = this.dialog.open(LoginComponent, { width: '250px', data: { username: this.login.username, password: this.login.password } });
  }

  ngOnInit() {
    
    if (this.auth.isLoggedIn())
      this.router.navigate(['/home']);
  }
}

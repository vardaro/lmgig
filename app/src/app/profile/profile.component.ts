import { Component, OnInit, HostListener } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { AuthService } from '../services/auth.service';
import { AchieveService } from '../services/achieve.service';
import { Achievement } from '../achievements';
import { User } from '../user';
import { PostService } from '../services/post.service';
import { Task } from '../tasks';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})

export class ProfileComponent implements OnInit {

  user: User;
  cols: number = 3;
  rows: number = 25;
  colorVar = 'white';
  percentage: number = 100;
  achievements: Achievement[];
  awards: number = Achievement.length;
  experience: number = this.awards * .5;

  chartData: number[];
  chartOptions = {
    responsive: true
  };

  chartLabels: string[];
  temp: Task[];
  onChartClick(event) {
    console.log(event);
  }

  constructor(
    private achieveService: AchieveService,
    private auth: AuthService,
    private postService: PostService,
  ) { }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.cols = this.calcCols();
    this.rows = this.calcRows();
  }

  ngOnInit() {
    this.achieveService.getAchieved().subscribe(achievements => this.achievements = achievements);
    this.cols = this.calcCols();
    this.rows = this.calcRows();
    this.auth.user$.subscribe(user => this.user = user);
    this.chartLabels = this.postService.getTypes();
    this.postService.getChartData().subscribe(data => this.chartData = data);
  }

  private calcCols(): number {
    return Math.ceil((window.innerWidth / 175) - 1);
  }

  private calcRows(): number {
    let num = Math.ceil(this.achievements.length / this.cols);
    if (num <= 0) { num = 1;}
    return num * 11;
  }

  onClick(pageName, other1, other2) {
    document.getElementById(other1).style.display = 'none';
    document.getElementById(other2).style.display = 'none';
    document.getElementById(pageName).style.display = 'block';
  }
}

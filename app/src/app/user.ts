export class User {
  PersonID: number;
  Domain: string;
  NTID: string;
  EmployeeID: string;
  DisplayName: string;
  City: string;
  JobTitle: string;
  Company: string;
  BusinessAreaName: string;
  Email: string;
  ManagerDisplayName: string;
  ManagerEmail: string;
  PhotoUrl: string;
  ProfileUrl: string;
  CurrentAssignment: string;
  Points: number;
}

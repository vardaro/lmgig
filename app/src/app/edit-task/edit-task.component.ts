import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PostService } from '../services/post.service';
import { Task } from '../tasks';

@Component({
  selector: 'app-dialog',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})

export class EditTaskComponent implements OnInit {

  task: Task;
  date: Date = new Date();
  boxCheck: boolean = false;
  locations: string[];
  types: string[];
  difficulties: string[];

  constructor(
    public dialogRef: MatDialogRef<EditTaskComponent>,
    private postService: PostService
  ) { }

  checkChange() {
    this.boxCheck = !this.boxCheck;
  }

  close() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.task.date = this.date.getMonth() + '/' + this.date.getDay() + '/' + this.date.getFullYear();
    this.postService.getEditableTask().subscribe(task => this.task = task);
    this.locations = this.postService.getLocations();
    this.types = this.postService.getTypes();
    this.difficulties = this.postService.getDifficulties();
  }
}

class datepick {
  myDate = new Date();
}

class checkBox {

}

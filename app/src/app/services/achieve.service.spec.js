"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var achieve_service_1 = require("./achieve.service");
describe('AchieveService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [achieve_service_1.AchieveService]
        });
    });
    it('should be created', testing_1.inject([achieve_service_1.AchieveService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=achieve.service.spec.js.map
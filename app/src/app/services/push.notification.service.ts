import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NOTIFICATIONS } from '../mock-notif';
import { Notification } from '../notification';
import { TASKS, Task } from '../tasks'

@Injectable({
  providedIn: 'root'
})
export class PushNotificationsService {
  notif = new Notification('', '', 0);
  constructor() { }
  notiff = new Notification('', '', 0);
  not: Notification[];
  num: number;
  numberOfNotificaiton() {
    let num = TASKS.length;
    return num;
  }

  sendNotification(notification) {

    NOTIFICATIONS.unshift(notification);
  }

  getNotifications(): Observable<Notification[]> {
    return of(NOTIFICATIONS);
  }

  notifPostTask(task, PersonID) {
    if (task.poc != '') {
      this.notif.name = task.poc;
      this.notif.text = 'posted a task';
      this.notif.PersonID = PersonID;
      NOTIFICATIONS.unshift(this.notif);
    } 
  }



  clear(): void {
    this.num = this.numberOfNotificaiton() - 1; 
    while (this.num >= 0) {
      NOTIFICATIONS.splice(this.num, 1);
      this.num = this.num - 1;
    }
  }
}

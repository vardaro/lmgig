import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, BehaviorSubject } from 'rxjs';
import {
  Task,
  TASKS,
  CLAIMED,
  POSTED,
  COMPLETED,
  LOCATIONS,
  TYPES,
  DIFFICULTIES
} from './../tasks';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  chartData = [0, 0, 0, 0, 0, 0, 0];
  editableTask = new Task('', '', '', '', '', '', '', false, '');

  constructor(private http: HttpClient) { }

  postTask(task) {
    TASKS.unshift(task);
    POSTED.push(task);
  }

  editTask(task) {
    this.editableTask = task;
  }

  getEditableTask(): Observable<Task> {
    return of(this.editableTask);
  }

  claimTask(task) {
    var index = TASKS.indexOf(task);
    if (index >= 0) {
      TASKS.splice(index, 1);
    }
    CLAIMED.push(task);
  }

  completeTask(task) {
    var index = POSTED.indexOf(task);
    if (index >= 0) {
      POSTED.splice(index, 1);
    }
    var index2 = CLAIMED.indexOf(task);
    if (index2 >= 0) {
      CLAIMED.splice(index2, 1);
    }
    COMPLETED.push(task);
    this.chartData[TYPES.indexOf(task.type)]++;
  }

  cancelPostedTask(task) {
    var index = POSTED.indexOf(task);
    if (index >= 0) {
      POSTED.splice(index, 1);
    }
    var index2 = CLAIMED.indexOf(task);
    if (index2 >= 0) {
      CLAIMED.splice(index2, 1);
    }
    var index3 = TASKS.indexOf(task);
    if (index3 >= 0) {
      TASKS.splice(index3, 1);
    }
  }

  cancelClaimedTask(task) {
    var index = CLAIMED.indexOf(task);
    if (index >= 0) {
      CLAIMED.splice(index, 1);
    }
    TASKS.push(task);
  }

  //The new improved fancy looking database read!
  getTasks(): Observable<any> {
    return this.http.get<any>('/tasks/read', { withCredentials: true });
  }

  //getTasks(): Observable<Task[]> {
  //  return of(TASKS);
  //}

  getClaimedTasks(): Observable<Task[]> {
    return of(CLAIMED);
  }

  getPostedTasks(): Observable<Task[]> {
    return of(POSTED);
  }

  getCompletedTasks(): Observable<Task[]> {
    return of(COMPLETED);
  }

  getLocations(): string[] {
    return LOCATIONS;
  }

  getTypes(): string[] {
    return TYPES;
  }

  getDifficulties(): string[] {
    return DIFFICULTIES;
  }
  
  getChartData(): Observable<number[]> {
    return of(this.chartData);
  }
}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Person } from './../person';

@Injectable({
  providedIn: 'root'
})

export class ProfileService {

  person: Person;

  setPerson(person) {
    this.person = person;
  }

  getPerson(): Observable<Person> {
    return of(this.person);
  }

  constructor() { }
}

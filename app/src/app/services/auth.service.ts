import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

import { User } from './../user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api = new BehaviorSubject<User>(null);
  user$ = this.api.asObservable();

  constructor(private http: HttpClient) { }

  private loginEndpoint: string = '/auth/login'
  user = false;

  /**
  * Makes a login attempt to api
  * withCredentials option must be true, otherwise server will throw a CORS error
  */
  authenticate(): Observable<User> {
    this.user = true;
    return this.http.get<User>(this.loginEndpoint, { withCredentials: true });
  }

  isLoggedIn(): boolean {
    return (this.user$) ? this.user : false;
  }

  setUser(data) {
    this.api.next(data);
  }
}

import { Injectable } from '@angular/core';
import { Tracker, Achievement, ACHIEVEMENTS, ACHIEVED } from '../achievements';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AchieveService {

  data: Tracker = new Tracker();
  constructor() { }

  update(): number {
    for (let c = 0; c < ACHIEVEMENTS.length; c++) {
      let item = ACHIEVEMENTS[c];
      item.update(this.data);
      if (item.completed && !item.achieved) {
        ACHIEVED.push(item);
        item.achieved = true;
      }
    }
    return this.data.numFinished;
  }

  getAchieved(): Observable<Achievement[]> {
    return of(ACHIEVED);
  }

  getAchievements(): Observable<Achievement[]> {
    return of(ACHIEVEMENTS);
  }

  decPosted() {
    this.data.numPosted--;
  }
  decClaimed() {
    this.data.numClaimed--;
  }

  incPosted() {
    this.data.numPosted++;
  }
  incClaimed() {
    this.data.numClaimed++;
  }
  incCompleted() {
    this.data.numCompleted++;
  }
  incExtra() {
    this.data.numExtra++;
  }
  incSimple() {
    this.data.numSimple++;
  }
  incModerate() {
    this.data.numModerate++;
  }
  incDifficult() {
    this.data.numDifficult++;
  }
  incDevelopment() {
    this.data.numDevelopment++;
  }
  incResearch() {
    this.data.numResearch++;
  }
  incBusiness() {
    this.data.numBusiness++;
  }
  incCommunication() {
    this.data.numCommunication++;
  }
  incIT() {
    this.data.numIT++;
  }
  incManagement() {
    this.data.numManagement++;
  }
  incSecret() {
    this.data.secret++;
  }

}

import { Component, OnInit } from '@angular/core';
import { Achievement, ACHIEVEMENTS } from '../achievements';
import { HeaderComponent } from '../header/header.component';
import { AchieveService } from '../services/achieve.service';
import { Leader } from '../leader';


@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  public completed: number = 0;
  public points: number = 0;
  achieved: number = 0;
  active: number = 0;
  leader: Leader;
  leaders: Leader[];

  leaderPerson = new Leader( '', '', '' );
 
  constructor(private achieveService: AchieveService,) { }

  ngOnInit() {

    console.log(this.points);

    }

  update(): number {
    var num = this.achieveService.update();
    this.active = num - this.achieved;
    this.completed = num;
    this.points = Math.round(this.completed * 2.27);
    return this.points;
   
  }

  viewAchievements(): void {
    this.achieved = this.update();
    this.active = 0;
  }
}

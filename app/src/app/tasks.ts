export class Task {
  constructor(
    public title: string,
    public type: string,
    public location: string,
    public date: string,
    public bandwidth: string,
    public poc: string,
    public description: string,
    public security: boolean,
    public difficulty: string
  ) { }
}

export const LOCATIONS: string[] = [
  'Phoenix, AZ',
  'Palmdale, CA',
  'San Jose, CA',
  'Sunnyvale, CA ',
  'Deer Creek, CO',
  'Denver Data Center, CO',
  'Waterton, CO',
  'Stratford, CT',
  'Trumbull, CT',
  'Ocala, FL',
  'Orlando, FL',
  'Marietta, GA',
  'New Orleans, LA',
  'Bethesda, MD',
  'Rockville, MD',
  'Melville, NY',
  'Owego, NY',
  'Cleveland, OH',
  'Aguadilla, PR',
  'Montreal, QC',
  'Austin, TX',
  'Dallas, TX',
  'Fort Worth, TX',
  'Arlington, VA',
  'Henderson, VA',
  'Manassas, VA',
  'Norfolk, VA',
  'Suffolk, VA',
  'Valley Forge, VA',
  'Virtual'

];

export const TYPES: string[] = [
  'Development',
  'Research',
  'Business',
  'Communication',
  'IT',
  'Project Management'
];

export const DIFFICULTIES: string[] = [
  'Simple',
  'Moderate',
  'Difficult'
];

export const TASKS: Task[] = [
  { title: 'Clockheed', type: 'Development', location: 'Deer Creek, CO', date: '6/11/2018', bandwidth: '65', poc: 'Chris Benik', description: 'Long live clockheed', security: true, difficulty: 'Simple' },
  { title: 'Martime', type: 'Business', location: 'Denver Data Center, CO', date: '4/23/2019', bandwidth: '50', poc: 'Brad Rokosz', description: 'Time to go home', security: true, difficulty: 'Moderate' },
  { title: 'Story Time', type: 'Communication', location: 'Waterton, CO', date: '10/30/2020', bandwidth: '100', poc: 'Shrek', description: 'Once upon a time there was a lovely princess.But she had an enchantment upon her of a fearful sort which could only be broken by loves first kiss. She was locked away in a castle guarded by a terrible fire - breathing dragon. Many brave knights had attempted to free her from this dreadful prison,but non prevailed.She waited in the dragons keep in the highest room of the tallest tower for her true love and true loves first kiss. Like thats ever gonna happen. ', security: false, difficulty: 'Difficult' },
  { title: 'Work', type: 'Research', location: 'Deer Creek, CO', date: '1/1/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },
  { title: 'AWS', type: 'Research', location: 'Deer Creek, CO', date: '2/24/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },
  { title: 'App Design', type: 'Development', location: 'Deer Creek, CO', date: '3/11/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },
  { title: 'Procurement', type: 'Business', location: 'Deer Creek, CO', date: '4/9/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },
  { title: 'Salary', type: 'Business', location: 'Deer Creek, CO', date: '5/29/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },
  { title: 'Govcloud', type: 'Research', location: 'Deer Creek, CO', date: '6/13/2018', bandwidth: '100', poc: '', description: 'Test', security: false, difficulty: 'Simple' },

];

export const CLAIMED: Task[] = [

];

export const POSTED: Task[] = [

];

export const COMPLETED: Task[] = [

];

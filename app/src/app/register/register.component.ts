import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'
import { ProfileService } from '../services/profile.service'
import { Person } from '../person';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  person = new Person('', '', '', '', '', '');

  constructor(
    public dialog: MatDialog,
    private profileService: ProfileService,
  ) { }

  openDialog(): void {
    //let dialogRef = this.dialog.open(LoginComponent, { width: '250px', data: { username: this.username, password: this.password } });
    this.profileService.setPerson(this.person);
    console.log(this.person);
  }

  ngOnInit() {
  }

  employeeType = [
    { value: 'Director' },
    { value: 'Manager' },
    { value: 'Employee' }
  ];

  locations = [
    { value: 'Deer Creek, CO' },
    { value: 'Denver Data Center, CO' },
    { value: 'Waterton, CO' },
    { value: 'Fort Worth, TX' },
    { value: 'Marietta, GA' },
    { value: 'Palmdale, CA' },
    { value: 'Valley Forge, VA' },
    { value: 'Bethesda, MD' },
    { value: 'Rockville, MD' },
    { value: 'Sunnyvale, CA ' },
    { value: 'Virtual' }
  ];

  types = [
    { value: 'Development' },
    { value: 'Research' },
    { value: 'Business' },
    { value: 'Communication' },
    { value: 'IT' },
    { value: 'Coffee' },
    { value: 'Socialization' },
  ];
}

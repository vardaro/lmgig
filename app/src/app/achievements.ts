export class Tracker {
  public numFinished: number = 0;
  public numPosted: number = 0;
  public numClaimed: number = 0;
  public numCompleted: number = 0;
  public numExtra: number = 0;
  public numSimple: number = 0;
  public numModerate: number = 0;
  public numDifficult: number = 0;
  public numDevelopment: number = 0;
  public numResearch: number = 0;
  public numBusiness: number = 0;
  public numCommunication: number = 0;
  public numIT: number = 0;
  public numManagement: number = 0;
  public secret: number = 0;
}

export class Requirement {
  private value: number = 0;
  private complete: boolean = false;

  constructor(
    private type: string,
    private goal: number
  ) { }

  public Value(): number {
    return this.value;
  }

  public Goal(): number {
    return this.goal;
  }

  public Complete(): boolean {
    return this.complete;
  }

  public update(data: Tracker) {
    if (this.type == "posted") {
      this.value = data.numPosted;
    }
    if (this.type == "claimed") {
      this.value = data.numClaimed;
    }
    if (this.type == "completed") {
      this.value = data.numCompleted;
    }
    if (this.type == "extra") {
      this.value = data.numExtra;
    }
    if (this.type == "simple") {
      this.value = data.numSimple;
    }
    if (this.type == "moderate") {
      this.value = data.numModerate;
    }
    if (this.type == "difficult") {
      this.value = data.numDifficult;
    }
    if (this.type == "secret") {
      this.value = data.secret;
    }
    if (this.type == "development") {
      this.value = data.numDevelopment;
    }
    if (this.type == "research") {
      this.value = data.numResearch;
    }
    if (this.type == "business") {
      this.value = data.numBusiness;
    }
    if (this.type == "communication") {
      this.value = data.numCommunication;
    }
    if (this.type == "IT") {
      this.value = data.numIT;
    }
    if (this.type == "management") {
      this.value = data.numManagement;
    }

    if (this.value >= this.goal) {
      this.complete = true;
      this.value = this.goal;
    }
  }
}

export class Achievement {

  public percentage: number = 0;
  public val: number = 0;
  public goal: number = 0;
  public completed: boolean = false;
  public achieved: boolean = false;
  private done: number = 0;

  constructor(
    public title: string,
    public description: string,
    public image: string,
    public REQUIREMENTS: Requirement[]
  ) {
    for (let c = 0; c < this.REQUIREMENTS.length; c++) {
      let req = this.REQUIREMENTS[c];
      this.goal += req.Goal();
    }
  }

  public complete(data: Tracker): boolean {
    if (!this.completed && this.REQUIREMENTS.length == 0) {
      this.completed = true;
      data.numFinished++;
      return true;
    }
    return false;
  }

  public update(data: Tracker) {
    
    if (!this.completed) {
      this.val = this.done;

      for (let c = 0; c < this.REQUIREMENTS.length; c++) {
        let req = this.REQUIREMENTS[c];

        if (!req.Complete()) {
          req.update(data);
        }
        if (req.Complete()) {
          this.done += req.Value();
          var index = this.REQUIREMENTS.indexOf(req);
          this.REQUIREMENTS.splice(index, 1);
          c--;
          this.complete(data);
        }
        this.val += req.Value();
      }
      this.percentage = (this.val / this.goal) * 100;
    }
  }
}

export const ACHIEVED: Achievement[] = [];

export const ACHIEVEMENTS: Achievement[] = [
  new Achievement('Post a task', 'Post a task.\nTime to get things done.', '../assets/achievement_post.png',
    [new Requirement('posted', 1)]),

  new Achievement('Adept Poster', 'Post a few tasks.\nAnd you get a task, and you get a task...', '../assets/achievement_post.png',
    [new Requirement('posted', 3)]),

  new Achievement('Posting Guru', 'Posting master.\nSpreading the love.', '../assets/achievement_post.png',
    [new Requirement('posted', 5)]),

  new Achievement('Claim a task', 'Claim a task.\nTime to get cracking!', '../assets/achievement_claim.png',
    [new Requirement('claimed', 1)]),

  new Achievement('Frequent Claimer', 'Claim a few tasks.\nSome serious initiative.', '../assets/achievement_claim.png',
    [new Requirement('claimed', 3)]),

  new Achievement('Claiming Enthusiast', 'Claiming master.\nLet\'s do this!', '../assets/achievement_claim.png',
    [new Requirement('claimed', 5)]),

  new Achievement('Complete a task', 'Complete a task.\nBoom baby!', '../assets/achievement_complete.png',
    [new Requirement('completed', 1)]),

  new Achievement('Consistant Completer', 'Complete a few tasks.\nOn to the next one.', '../assets/achievement_complete.png',
    [new Requirement('completed', 3)]),

  new Achievement('Completionist', 'Completing master.\nGetting things done.', '../assets/achievement_complete.png',
    [new Requirement('completed', 5)]),

  new Achievement('Full Process', 'Post a task. Claim a task. Complete a task.', '../assets/achievement_full_process.png',
    [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1)]),

  new Achievement('Creative Genius', 'Changed the theme.\n Look at all those pretty colors.', '../assets/achievement_change_theme.png',
    [new Requirement('extra', 1)]),

  new Achievement('Slim Shady', 'The real Slim Shady.\nFind a secret.', '../assets/achievement_slim_shady.png',
    [new Requirement('secret', 1)]),

  new Achievement('Simply Amazing', 'Complete a simple task', '../assets/achievement_simple.png',
    [new Requirement('simple', 1)]),

  new Achievement('Moderately Marvelous', 'Complete a moderate task', '../assets/achievement_moderate.png',
    [new Requirement('moderate', 1)]),

  new Achievement('Difficult Terrain', 'Complete a difficult task', '../assets/achievement_difficult.png',
    [new Requirement('difficult', 1)]),

  new Achievement('Padawan to Jedi', 'Complete a simple, moderate, and difficult task', '../assets/achievement_jedi.png',
    [new Requirement('simple', 1), new Requirement('moderate', 1), new Requirement('difficult', 1)]),

  new Achievement('Development', 'Complete a task from the Development category', '../assets/achievement_development.png',
    [new Requirement('development', 1)]),

  new Achievement('Research', 'Complete a task from the Research category', '../assets/achievement_research.png',
    [new Requirement('research', 1)]),

  new Achievement('Business', 'Complete a task from the Business category', '../assets/achievement_business.png',
    [new Requirement('business', 1)]),

  new Achievement('Communication', 'Complete a task from the Communication category', '../assets/achievement_communication.png',
    [new Requirement('communication', 1)]),

  new Achievement('IT', 'Complete a task from the IT category', '../assets/achievement_plug.png',
    [new Requirement('IT', 1)]),

  new Achievement('Project Management', 'Complete a task from the Project Management category', '../assets/achievement_lightbulb.png',
    [new Requirement('management', 1)]),

  new Achievement('Jack of all Trades', 'Complete a task from each of the categories', '../assets/achievement_wrench.png',
    [new Requirement('development', 1), new Requirement('research', 1), new Requirement('business', 1), new Requirement('communication', 1), new Requirement('IT', 1), new Requirement('management', 1)]),


  new Achievement('Achieve Enlightenment', 'Do everything.', '../assets/achievement_enlightenment.png',
    [new Requirement('posted', 1), new Requirement('claimed', 1), new Requirement('completed', 1), new Requirement('extra', 1), new Requirement('secret', 1),
      new Requirement('simple', 1), new Requirement('moderate', 1), new Requirement('difficult', 1),
      new Requirement('development', 1), new Requirement('research', 1), new Requirement('business', 1), new Requirement('communication', 1), new Requirement('IT', 1), new Requirement('management', 1)]),

  //Make symbol and change requirements for these achievements

  new Achievement('Feedback', 'Send us a message about what you think of the app!', '../assets/achievement_enlightenment.png',
    [new Requirement('posted', 1)])

];

export class Person {
  constructor(
    public name: string,
    public email: string,
    public password: string,
    public department: string,
    public employee: string,
    public location: string) {}
}

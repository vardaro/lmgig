import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { PostService } from '../services/post.service';
import { ProfileService } from '../services/profile.service';
import { AchieveService } from '../services/achieve.service';
import { Task } from '../tasks';
import { AuthService } from '../services/auth.service';
import { PushNotificationsService } from '../services/push.notification.service';
import { User } from '../user';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent implements OnInit {

  user: User;
  task = new Task('', '', '', '', '', '', '', false, '');
  date: Date = new Date();
  poc = '';
  boxCheck: boolean = false;

  locations: string[];
  types: string[];
  difficulties: string[];

  checkChange() {
    this.boxCheck = true;
  }

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    private postService: PostService,
    private profileService: ProfileService,
    private achieveService: AchieveService,
    private auth: AuthService,
    private pushNotifcation: PushNotificationsService
  ) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => this.poc = user.DisplayName);
    this.auth.user$.subscribe(user => this.user = user);
    this.locations = this.postService.getLocations();
    this.types = this.postService.getTypes();
    this.difficulties = this.postService.getDifficulties();
  }

  close() {
    this.task.poc = this.poc;
    this.task.date = this.date.getMonth() + '/' + this.date.getDay() + '/' + this.date.getFullYear();
    this.postService.postTask(this.task);
    this.dialogRef.close();
    this.achieveService.incPosted();
    console.log('check');
    this.pushNotifcation.notifPostTask(this.task, this.user.PersonID);
  }


}

class datepick {
  myDate = new Date();
}

class checkBox {

}

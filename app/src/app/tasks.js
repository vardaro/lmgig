"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Task = /** @class */ (function () {
    function Task(title, type, location, duration, bandwidth, poc, description, security, difficulty) {
        this.title = title;
        this.type = type;
        this.location = location;
        this.duration = duration;
        this.bandwidth = bandwidth;
        this.poc = poc;
        this.description = description;
        this.security = security;
        this.difficulty = difficulty;
    }
    return Task;
}());
exports.Task = Task;
exports.LOCATIONS = [
    'Phoenix, AZ',
    'Palmdale, CA',
    'San Jose, CA',
    'Sunnyvale, CA ',
    'Deer Creek, CO',
    'Denver Data Center, CO',
    'Waterton, CO',
    'Stratford, CT',
    'Trumbull, CT',
    'Ocala, FL',
    'Orlando, FL',
    'Marietta, GA',
    'New Orleans, LA',
    'Bethesda, MD',
    'Rockville, MD',
    'Melville, NY',
    'Owego, NY',
    'Cleveland, OH',
    'Aguadilla, PR',
    'Montreal, QC',
    'Austin, TX',
    'Dallas, TX',
    'Fort Worth, TX',
    'Arlington, VA',
    'Henderson, VA',
    'Manassas, VA',
    'Norfolk, VA',
    'Suffolk, VA',
    'Valley Forge, VA',
    'Virtual'
];
exports.TYPES = [
    'Development',
    'Research',
    'Business',
    'Communication',
    'IT',
    'Socialization',
    'Project Management'
];
exports.DIFFICULTIES = [
    'Simple',
    'Moderate',
    'Difficult'
];
exports.TASKS = [
  { title: 'Clockheed', type: 'Development', location: 'Deer Creek, CO', duration: '03/14/2019', bandwidth: '65', poc: 'Chris Benik', description: 'Long live clockheed', security: true, difficulty: 'Simple' },
    { title: 'Martime', type: 'Business', location: 'Denver Data Center, CO', duration: '03/14/2019', bandwidth: '50', poc: 'Brad Rokosz', description: 'Time to go home', security: true, difficulty: 'Moderate' },
    { title: 'Story Time', type: 'Socialization', location: 'Waterton, CO', duration: '10/31/2020', bandwidth: '100', poc: 'Shrek', description: 'Once upon a time there was a lovely princess.But she had an enchantment upon her of a fearful sort which could only be broken by loves first kiss. She was locked away in a castle guarded by a terrible fire - breathing dragon. Many brave knights had attempted to free her from this dreadful prison,but non prevailed.She waited in the dragons keep in the highest room of the tallest tower for her true love and true loves first kiss. Like thats ever gonna happen. ', security: false, difficulty: 'Difficult' }
];
exports.CLAIMED = [];
exports.POSTED = [];
exports.COMPLETED = [];
//# sourceMappingURL=tasks.js.map

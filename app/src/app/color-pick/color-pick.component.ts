import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@Component({
  selector: 'app-color-pick',
  templateUrl: './color-pick.component.html',
  styleUrls: ['./color-pick.component.css']
})
export class ColorPickComponent implements OnInit {

  test: string = "";

  constructor(
    public dialogRef: MatDialogRef<ColorPickComponent>
  ) {

  }

  selectColor(color) {
    for (let i = 0; i < COLORS.length; i++)
    {
      var theme = COLORS[i];
      if (theme.name == color) {
        document.documentElement.style.setProperty("--primary", theme.primary);
        document.documentElement.style.setProperty("--accent", theme.accent);
        document.documentElement.style.setProperty("--hover", theme.hover);
      }
     
    }
  }

  saveTheme() {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}

export class Color {

  constructor(
    public name,
    public primary,
    public accent,
    public hover
  ) { }
}

export const COLORS: Color[] = [
  new Color('red', 'rgb(200, 14, 34)', 'rgb(220, 85, 95)', 'rgb(249, 204, 204)'),
  new Color('orange', 'rgb(229, 64, 9)', 'rgb(255, 130, 90)', 'rgb(255, 214, 186)'),
  new Color('amber', 'rgb(252,180, 0)', 'rgb(255, 210, 85)', 'rgb(252, 245, 185)'),
  new Color('grey', 'rgb(50, 50, 70)', 'rgb(100, 100, 120)', 'rgb(200, 200, 220)'),
  new Color('lime', 'rgb(94, 255, 64)', 'rgb(145, 255, 145)', 'rgb(235, 255, 215)'),
  new Color('green', 'rgb(18, 168, 23)', 'rgb(120, 225, 80)', 'rgb(210, 255, 170)'),
  new Color('aqua', 'rgb(0, 201, 167)', 'rgb(78, 240, 200)', 'rgb(210, 255, 235)'),
  new Color('teal', 'rgb(0, 128, 128)', 'rgb(60, 200, 200)', 'rgb(200, 255, 255)'),
  new Color('light blue', 'rgb(0, 151, 201)', 'rgb(140, 210, 250)', 'rgb(205, 235, 268)'),
  new Color('blue', 'rgb(3, 43, 163)', 'rgb(137, 170, 279)', 'rgb(200, 200, 280)'),
  new Color('purple', 'rgb(100, 3, 163)', 'rgb(150, 100, 210)', 'rgb(215, 190, 277)'),
  new Color('navy', 'rgb(34, 4, 115)', 'rgb(145, 140, 200)','rgb(200, 195, 230)'),
  new Color('pink', 'rgb(249, 49, 116)',  'rgb(255, 120, 156)','rgb(255, 190, 210)'),
  new Color('maroon', 'rgb(161, 30, 75)', 'rgb(237, 130, 170)', 'rgb(255, 200, 230)'),
  new Color('fuschia', 'rgb(167, 0, 186)', 'rgb(210, 120, 220)', 'rgb(269, 210, 282)'),
  new Color('purple2', 'rgb(80, 1, 81)', 'rgb(200, 130, 200)', 'rgb(255, 220, 255)')
];




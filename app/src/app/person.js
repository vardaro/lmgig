"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Person = /** @class */ (function () {
    function Person(name, email, password, department, location) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.department = department;
        this.location = location;
    }
    return Person;
}());
exports.Person = Person;
//# sourceMappingURL=person.js.map
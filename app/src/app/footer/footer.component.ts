import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AchieveService } from '../services/achieve.service';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(
    private achieveService: AchieveService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  )
  {
    this.matIconRegistry.addSvgIcon(
      "eureka",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/eureka.svg")
    );
  }

  ngOnInit() {
  }

  found() {
    this.achieveService.incSecret();
  }

}

export class Notification {
  constructor(
    public name: string,
    public text: string,
    public PersonID: number,
  ) { }
}

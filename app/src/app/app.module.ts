import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';



import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DialogComponent } from './dialog/dialog.component';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { RegisterComponent } from './register/register.component';
import { ColorPickComponent } from './color-pick/color-pick.component';
import { HeaderComponent } from './header/header.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { ProfileComponent } from './profile/profile.component';
import { TasksComponent } from './tasks/tasks.component';
import { CdkTableModule } from '@angular/cdk/table';
import { LogoutComponent } from './logout/logout.component';
import { filterDifficultyPipe, filterLocationPipe, filterCategoryPipe } from './home/home.pipe';



import { PostService } from './services/post.service';
import { ProfileService } from './services/profile.service';
import { AchieveService } from './services/achieve.service';

import {
  MatToolbarModule,
  MatMenuModule,
  MatButtonModule,
  MatDialogModule,
  MatListModule,
  MatInputModule,
  MatPaginatorModule,
  MatCheckboxModule,
  MatCardModule,
  MatIconModule,
  MatDividerModule,
  MatSelectModule,
  MatTabsModule,
  MatSidenavModule,
  MatGridListModule,
  MatTableModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatNativeDateModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { FooterComponent } from './footer/footer.component';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { QuestionsComponent } from './questions/questions.component';
import { JokeComponent } from './joke/joke.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DialogComponent,
    RegisterComponent,
    ProfileComponent,
    TasksComponent,
    LogoutComponent,
    HeaderComponent,
    ColorPickComponent,
    AchievementsComponent,
    filterCategoryPipe,
    filterLocationPipe,
    filterDifficultyPipe,
    FooterComponent,
    LeaderboardComponent,
    AboutComponent,
    ContactComponent,
    QuestionsComponent,
    JokeComponent,
    EditTaskComponent

  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    MatButtonToggleModule,
    MatMenuModule,
    MatButtonModule,
    MatDialogModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    CdkTableModule,
    BrowserAnimationsModule,
    MatListModule,
    MatInputModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatCardModule,
    MatDividerModule,
    MatTableModule,
    MatSelectModule,
    MatTabsModule,
    MatIconModule,
    MatSidenavModule,
    MatGridListModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatBadgeModule,
    MatProgressBarModule,
    HttpClientModule,
    ChartsModule,
    MatExpansionModule
  ],
  providers: [PostService, ProfileService, AchieveService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent, EditTaskComponent, ColorPickComponent]
})
export class AppModule { }



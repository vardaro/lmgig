import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { User } from './user';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'app';
  data: User;
  constructor(private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.auth.authenticate().subscribe(data => {
      this.data = data
      console.log(data);
      this.auth.setUser(data);
    }, err => { console.log(err) }
    );

  }
}

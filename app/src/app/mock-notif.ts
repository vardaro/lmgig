import { Notification } from './notification';

export const NOTIFICATIONS: Notification[] = [
  { name: 'John King', text: 'posted a task', PersonID: 0 },
  { name: 'Peter Parker', text: 'claimed a task',  PersonID: 0},
  { name: 'Tom Smith', text: 'has recommended a task',  PersonID: 0},
  { name: 'You', text: 'have earned an acheivement for posting a task',  PersonID: 0},
  { name: 'Clockheed', text: 'task deadline is approaching',  PersonID: 0},
];



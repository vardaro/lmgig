import { Component, OnInit, HostListener } from '@angular/core';
import { AchieveService } from '../services/achieve.service';
import { Achievement } from '../achievements';

@Component({
  selector: 'app-achievements',
  templateUrl: './achievements.component.html',
  styleUrls: ['./achievements.component.css']
})
export class AchievementsComponent implements OnInit {

  constructor(
    private achieveService: AchieveService
  ) { }

  cols: number = 3;
  colorVar = 'white';
  percentage: number = 100;

  achievements: Achievement[];

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.cols = ((window.innerWidth) / 400);
  }

  ngOnInit() {
    this.cols = ((window.innerWidth) / 400);
    this.achieveService.getAchievements().subscribe(achievements => this.achievements = achievements);
  }

  post() {
    this.achieveService.incPosted();
    this.achieveService.update();
  }

  claim() {
    this.achieveService.incClaimed();
    this.achieveService.update();
  }

  complete() {
    this.achieveService.incCompleted();
    this.achieveService.update();
  }

  extra() {
    this.achieveService.incExtra();
    this.achieveService.update();
  }
 
}

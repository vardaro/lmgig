"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Leader = /** @class */ (function () {
    function Leader(position, name, points) {
        this.position = position;
        this.name = name;
        this.points = points;
    }
    return Leader;
}());
exports.Leader = Leader;
exports.LEADER = [
    { position: '1', name: 'Carl Fredricksen', points: '32,294' },
    { position: '2', name: 'Li Shang', points: '31,457' },
    { position: '3', name: 'Cobra Bubbles', points: '29,879' },
    { position: '4', name: 'Lilo Pelekai', points: '29,776' },
    { position: '5', name: 'Vincenzo Santorini', points: '28,394' }
];
//# sourceMappingURL=leader.js.map
export class Leader {
  constructor(
    public position: string,
    public name: string,
    public points: string,
   
  ) { }
}

export const LEADER: Leader[] = [
  { position: '1', name: 'Carl Fredricksen', points: '32,294' },
  { position: '2', name: 'Li Shang', points: '31,457' },
  { position: '3', name: 'Cobra Bubbles', points: '29,879' },
  { position: '4', name: 'Lilo Pelekai', points: '29,776' },
  { position: '5', name: 'Vincenzo Santorini', points:'28,394'}

];

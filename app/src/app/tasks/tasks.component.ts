import { Component, OnInit } from '@angular/core';
import { Task } from '../tasks';
import { PostService } from '../services/post.service';
import { AchieveService } from '../services/achieve.service';
import { RouterLink } from '@angular/router';
import { MatDialog, MAT_DIALOG_DATA, MatTabChangeEvent } from "@angular/material";
import { EditTaskComponent } from '../edit-task/edit-task.component';

export interface SelectedDialogData {
  title: 'hello';
}

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  claimedTasks: Task[];
  postedTasks: Task[];
  completedTasks: Task[];
  claim: Task;
  post: Task;
  complete: Task;
  selected: boolean = false;

  restoreTask = new Task('', '', '', null, '', '', '', false, 'Simple');

  selectedClaim = new Task('', '', '', null, '', '', 'Task Description', false, 'Simple');
  selectedPost = new Task('', '', '', null, '', '', 'Task Description', false, 'Simple');
  selectedComplete = new Task('', '', '', null, '', '', 'Task Description', false, 'Simple');

  completeDisabled: boolean = true;
  cancelDisabled: boolean = true;
  changeDisabled: boolean = true;

  tab: number = 0;

  constructor(
    private postService: PostService,
    private achieveService: AchieveService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.postService.getClaimedTasks().subscribe(tasks => this.claimedTasks = tasks);
    this.postService.getPostedTasks().subscribe(tasks => this.postedTasks = tasks);
    this.postService.getCompletedTasks().subscribe(tasks => this.completedTasks = tasks);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.tab = tabChangeEvent.index;
  }

  close() {
    this.selected = false;
  }

  //Claimed tab methods
  selectClaim(task: Task): void {
    if (this.selectedClaim != task) {
      this.selectedClaim = task;
      this.completeDisabled = false;
      this.selected = true;
    }
    else {
      this.selectedClaim = this.restoreTask;
      this.selected = false;
      this.completeDisabled = true;
    }
  }

  completeTask(task: Task): void {
    this.postService.completeTask(task);
    this.completeDisabled = true;
    this.selected = false;
    this.selectedClaim = this.restoreTask;
    this.achieveService.incCompleted();
    this.trackCompletetions(task);
  }

  cancelClaimedTask(task: Task): void {
    this.postService.cancelClaimedTask(task);
    this.completeDisabled = true;
    this.selected = false;
    this.selectedClaim = this.restoreTask;
    this.achieveService.decClaimed();
  }

  trackCompletetions(task: Task): void {
    if (task.difficulty == "Simple") this.achieveService.incSimple();
    else if (task.difficulty == "Moderate") this.achieveService.incModerate();
    else if (task.difficulty == "Difficult") this.achieveService.incDifficult();

    if (task.type == "Development") this.achieveService.incDevelopment();
    else if (task.type == "Research") this.achieveService.incResearch();
    else if (task.type == "Business") this.achieveService.incBusiness();
    else if (task.type == "Communication") this.achieveService.incCommunication();
    else if (task.type == "IT") this.achieveService.incIT();
    else if (task.type == "Project Management") this.achieveService.incManagement();
  }

  //Posted tab methods
  selectPost(task: Task): void {
    if (this.selectedPost != task) {
      this.selectedPost = task;
      this.cancelDisabled = false;
      this.selected = true;
      this.changeDisabled = false;
    }
    else {
      this.selectedClaim = this.restoreTask;
      this.selected = false;
      this.cancelDisabled = true;
    }
  }

  cancelPostedTask(task: Task): void {
    this.postService.cancelPostedTask(task);
    this.cancelDisabled = true;
    this.selectedClaim = this.restoreTask;
    this.selected = false;
    this.achieveService.decPosted();
  }

  changePostedTask(task: Task): void {
    this.postService.editTask(task);
    let dialog = this.dialog.open(EditTaskComponent);
  }

  //Completed tab methods
  selectComplete(task: Task): void {
    if (this.selectedComplete != task) {
      this.selectedComplete = task;
      this.selected = true;
    }
    else {
      this.selectedComplete = this.restoreTask;
      this.selected = false;
    }
  }

  
}

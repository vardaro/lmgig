import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PostService } from '../services/post.service';
import { AchieveService } from '../services/achieve.service';
import { Task, TASKS } from '../tasks';
import { User } from '../user';
import { AuthService } from '../services/auth.service';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  welcome: string = "GIG is an app that allows managers to post tasks they need completed, enabling employees to claim and complete said tasks. So click on a task and click the Claim Task button to begin!"
  tasks: Task[] = new Array<Task>();
  task: Task;
  user: User;
  pipeCategory: string = "Any Category";
  pipeLocation: string = "Any Location";
  pipeDifficulty: string = "Any Difficulty";
  categories: string[] = ["Any Category"];
  locations: string[] = ["Any Location"];
  difficulties: string[] = ["Any Difficulty"];

  selectedTask = new Task('', '', '', null, '', '', this.welcome, false, 'Simple');
  restoreTask = new Task('', '', '', null, '', '', this.welcome, false, 'Simple');

  buttonDisabled: boolean = true;
  selected: boolean = false;

  poc = '';

  constructor(
    public dialog: MatDialog,
    private postService: PostService,
    private achieveService: AchieveService,
    private auth: AuthService
  ) { }

  select(task: Task): void {
    if (this.selectedTask != task) {
      this.selectedTask = task;
      this.selected = true;
      this.buttonDisabled = false;
    }
    else {
      this.selectedTask = this.restoreTask;
      this.selected = false;
      this.buttonDisabled = true;
    }
    if (task.poc === this.user.DisplayName) {
      this.buttonDisabled = true;
    }
  }

  data: any;

  claim(task: Task): void {
    this.postService.claimTask(task);
    this.buttonDisabled = true;
    this.achieveService.incClaimed();
    this.selected = false;
  }
  close() {
    this.selected = false;
  }
 
  
  ngOnInit() {
	  //for "ng serve --open"
	  this.tasks = this.tasks.concat(TASKS);
	
    this.auth.user$.subscribe(user => this.user = user);
    this.categories = this.categories.concat(this.postService.getTypes());
    this.locations = this.locations.concat(this.postService.getLocations());
    this.difficulties = this.difficulties.concat(this.postService.getDifficulties());
    this.postService.getTasks().subscribe(data => {
      this.data = data;
      this.convert();
    });
  }

  convert(): void {
    let list: any[] = this.data.read;

    for (let c = 0; c < list.length; c++) {
      let item = list[c];
      console.log(item.title);
      console.log(item.type);
      console.log(item.location);
      console.log(item.date);
      console.log(item.bandwidth);
      console.log(item.poc);
      console.log(item.description);
      console.log(item.security);
      console.log(item.difficulty);

      this.tasks.push(new Task(item.title, item.type, item.location, item.date, item.bandwidth, item.poc, item.description, item.security, item.difficulty));
    }
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../tasks';

@Pipe({ name: 'filterCategory', pure: false })
export class filterCategoryPipe implements PipeTransform {
  transform(tasks: Task[], category: string) {
    if (category == "Any Category"){
      return tasks;
    }
    else {
      return tasks.filter(task => (task.type == category));
    }
  }
}

@Pipe({ name: 'filterLocation', pure: false })
export class filterLocationPipe implements PipeTransform {
  transform(tasks: Task[], location: string) {
    if (location == "Any Location") {
      return tasks;
    }
    else {
      return tasks.filter(task => (task.location == location));
    }
  }
}

@Pipe({ name: 'filterDifficulty', pure: false })
export class filterDifficultyPipe implements PipeTransform {
  transform(tasks: Task[], difficulty: string) {
    if (difficulty == "Any Difficulty") {
      return tasks;
    }
    else {
      return tasks.filter(task => (task.difficulty == difficulty));
    }
  }
}


